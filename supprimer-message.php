<?php
session_start();

if($_SESSION["utilisateur"]["statut"] != 1){
    header("Location: index.php");
}
$id = strip_tags($_GET["id"]);

include_once "pdo.php";

$count = $pdo->prepare("DELETE FROM contact WHERE id=:id");
$count->bindParam(":id",$id);
$count->execute();

header("Location: admin-contact.php");
?>
