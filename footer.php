<footer>
    <span>Copyright © 2022 Rucher-du-Pillier FERLAY.T tous droits réservés.</span>
    <span>Rucher du Pillier, apiculteur dans le 42800.</span>
    <ul>
        <li><a href="https://www.instagram.com/rucher_du_pillier/">Instagram</a></li>
        <li><a href="https://www.facebook.com/ferlay.didier">Facebook</a></li>
        <li><a href="https://twitter.com/moudchaux">Twitter</a></li>
        <li><a href="credit.php">Crédits</a></li>
    </ul>
</footer>
