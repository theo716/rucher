<?php
session_start();

if(!empty($_SESSION["utilisateur"])){
	if($_SESSION["utilisateur"]["statut"] == 1){
		header("Location: admin.php");
	}else{
		header("Location: profil.php");
	}

}
//Vérifie si le formulaire n'est pas vide
if(!empty($_POST)){
	//Vérifie  qu'il y a un email et un mot de passe et que les champs ne sont pas vides
	if(isset($_POST["email"], $_POST["pass"]) && !empty($_POST["email"]) && !empty($_POST["pass"])){

		//Vérification de l'email avec la méthode FILTER
		if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
			$message_echec = "Adresse email invalide"; 
		}else{

			//Enregistrement
			require_once "pdo.php";

			$email = strip_tags($_POST["email"]);
			//Requete qui permet de récuperer l'utilisateur avec son email
			$query = $pdo->prepare("SELECT * FROM utilisateurs WHERE email = :email");
			$query->bindParam(":email",$email);
			$query->execute();
			$utilisateur = $query->fetch();

			//Si l'utilisateur n'est pas en base de données alors on stop
			if(!$utilisateur){
				$message_echec = "Votre compte n'existe pas!";
			}else{

				//Vérification du mot de passe si il corresponds bien
				if(!password_verify($_POST["pass"], $utilisateur["pass"])){
					$message_echec = "Email ou Mot de passe invalide!";
				}else{

					//On stock en SESSION les informations nécessaires
					$_SESSION["utilisateur"] = [
						"id" => $utilisateur["id"],
						"prenom" => $utilisateur["prenom"],
						"nom_de_famille" => $utilisateur["nom_de_famille"],
						"email" => $utilisateur["email"],
						"statut" => $utilisateur["statut"]
					];

					//Et on redirige vers la page une fois la connexion effectuée
					header("Location: profil.php");
				}
			}
		}
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Connexion</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
<?php include_once "nav.php"; ?>

    <div id="conteneur-connexion-inscription">
    <h1>Connexion</h1>
    <form method="post">
	<div class="email">
	<label for="email">Email*</label>
	<input type="email" id="email" name="email" require></input>
	</div>

	<div class="pass">
	<label for="pass">Mot de passe*</label>
	<input type="password" id="pass" name="pass" require></input>
	</div>

	<button type="submit">Connexion</button>
    </form>
    <a href="inscription.php">Inscrivez-vous</a>

<?php
if(isset($message_echec)){
	echo "<a class='echec'> $message_echec </a>";
}
if(isset($message_succes)){
	echo "<a class='succes'> $message_succes </a>";
}
?>
    </div>
</body>
</html>
