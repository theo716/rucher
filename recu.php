<?php

session_start();

include_once "pdo.php";

$id_commande = strip_tags($_GET['id_commande']);
$id_client = strip_tags($_GET['id_client']);

if ($_SESSION['utilisateur']['id'] != $id_client) {
	header("Location: index.php");
}
?>
<!DOCTYPE html>
<html>

<head>
	<title>Recu de <?php echo $_SESSION['utilisateur']['nom_de_famille'];
					echo " ";
					echo $_SESSION['utilisateur']['prenom']; ?> </title>
	<meta charset='utf-8' />
	<link rel="stylesheet" href="styles.css">
</head>

<body>
	<?php
	if (is_numeric($id_client) && is_numeric($id_commande)) {

		$query = $pdo->prepare('SELECT * FROM reservation WHERE id = :id_commande');
		$query->bindValue(":id_commande", $id_commande);
		$query->execute();
		$res = $query->fetch();
	?>

		<div id="conteneur-global-recu">

		<?php
		if ($res && ($id_client == $res['id_client'])) {
			echo '<div id="conteneur-recu">
<h2>' . $_SESSION['utilisateur']['prenom'] . ' ' . $_SESSION['utilisateur']['nom_de_famille'] . ', numéro de commande: ' . $id_commande . '</h2><br /><h2 id="recap-commande-titre">Le récapitulatif de votre commande est ci-dessous:</h2>';
			echo '<div id="recapitulatif-commande">
	-' . $res['q1'] . ' pot(s) de 250 grammes, <br /> -' . $res['q2'] . ' pot(s) de 500 grammes, <br />-' . $res['q3'] . ' pot(s) de 1 kilogramme, <br /> pour un total de ' . $res['total'] . ' pot(s) de miel ! <br /></div>';
			echo '<div id="merci"><h3>Vous pouvez suivre le cheminement de votre commande sur votre profil.</h3><br><h3>Merci pour votre réservation, veuillez prendre contact avec moi par téléphone ou par mail pour venir récupérer votre commande.</h3></div>';

			echo '<div id="bas-recu"><input type="button" value="imprimer" onclick="window.print();" /><a href="index.php">Retour</a> </div></div>';
		} else {
			header("Location: index.php");
		}
	} else {
		header("Location: index.php");
	}

		?>
		</div>
</body>

</html>