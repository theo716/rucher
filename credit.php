<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Crédits</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
</head>
<?php include_once "nav.php" ?>

<body>
	<div id="credit">
		<p>Développement par FERLAY.T 🐝 dans le cadre d'un projet de l'Unité d'enseignement développement web (Licence 2 Informatique Université Jean Monnet Saint-Etienne)</p>
		<p>Pour me contacter merci de laisser un message, <a href="index.php#contact">ici</a>.</p>
		<a href="https://www.flaticon.com/fr/icones-gratuites/abeille" title="abeille icônes">Abeille icônes créées par Freepik - Flaticon</a>
		<a href="https://pixabay.com/fr/photos/abeille-fleur-macro-f%c3%a9conder-1726659/">fond image site</a>
	</div>
</body>

</html>