<?php
session_start();
if (!isset($_SESSION["utilisateur"])) {
	header("Location: connexion.php");
}


if ($_SESSION["utilisateur"]["statut"] == 1) {
	header("Location: admin.php");
}
include_once "pdo.php";
include_once "nav.php";

$nom = $_SESSION['utilisateur']['nom_de_famille'];
$prenom = $_SESSION['utilisateur']['prenom'];
$id = $_SESSION['utilisateur']['id'];
?>

<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF-8">
	<title>Votre profil</title>
	<link rel="stylesheet" href="styles.css">
</head>

<body>
	<div id="conteneur-profil">
		<h2>Votre profil</h2>
		<div id="conteneur-log">

			<h3 class="champ">Nom: <a><?php echo $_SESSION['utilisateur']['nom_de_famille']; ?></a></h3>
			<h3 class="champ">Prénom: <a><?php echo $_SESSION['utilisateur']['prenom']; ?></a></h3>
			<h3 class="champ">Email: <a><?php echo $_SESSION['utilisateur']['email']; ?></a></h3>
			<a href="modifier-mdp.php">Modifier votre mot de passe</a>
			<br>
			<a href="modifier-profil.php">Modifier votre profil</a>

		</div>
		<div id="conteneur-reservation">
			<?php
			$sql = "SELECT * FROM reservation WHERE id_client=$id";
			?>
				<h3>Vos réservations</h3>

				<table>
					<tr>
						<th>250g</th>
						<th>500g</th>
						<th>1000g</th>
						<th>Total</th>
						<th>Etat</th>
						<?php
						foreach ($pdo->query($sql) as $row) {

							echo "<tr>";
							print '<td>' . $row['q1'] . '</td>';
							print '<td>' . $row['q2'] . '</td>';
							print '<td>' . $row['q3'] . '</td>';
							print '<td>' . $row['total'] . '</td>';
							if ($row['etat'] == 0) {
								print '<td> <a class="verification">en vérification</a></td>';
							}
							if ($row['etat'] == 1) {
								print '<td class="cours">en cours de traitement</td>';
							}
							if ($row['etat'] == 2) {
								print '<td class="valide">terminé</td>';
							}
						}
						echo "</tr>";

						?>
					</tr>
				</table>
		</div>
	</div>
</body>

</html>