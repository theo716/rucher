<?php
session_start();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="styles.css">
    <title>Questions</title>
</head>
<body>
<?php include_once "nav.php" ?>
    <div id="grp-question">
        <h2 id="titre-questions"> Vos questions </h1>
        <div id="panneau" onclick="afficher('reponse1')">
            <div id="question">
                <span>Quelle est l'origine du miel vendu ?</span>
            </div>
            <div id="reponse1">
                <span>Il est produit à Chateauneuf dans le cadre de la législation.</span>
            </div>
        </div>

        <div id="panneau" onclick="afficher('reponse2')">
        <div id="question">
                <span>Qui suis-je ?</span>
            </div>
            <div id="reponse2">
                <span>Didier FERLAY Apiculteur dans la Loire 42, passionné de la vie des colonies d'abeilles.</span>
            </div>
        </div>

        <div id="panneau" onclick="afficher('reponse3')">
        <div id="question">
                <span>Comment se procurer du miel ?</span>
            </div>
            <div id="reponse3">
                <span>Sur ce site, il est possible de réserver votre miel, et par la suite de prendre contact avec moi pour payer et récupérer votre commande. Ou bien pour réserver vous pouvez passer directement par mon contact.</span>
            </div>
        </div>
        <a id="button-nav" href="index.php">Retour</a>
    </div>
</body>

<script>
    function afficher(id){
        document.getElementById(id).style.display = 'block';
    }
</script>
</html>
