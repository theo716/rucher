<?php 

session_start();

//Détruit la session en cours.
unset($_SESSION["utilisateur"]);

header("Location: index.php");
?>