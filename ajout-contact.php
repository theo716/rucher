<?php

session_start();

include_once 'pdo.php';

if(!empty($_POST)){
	if(isset($_POST["email"],$_POST["message"]) && !empty($_POST["email"]) && !empty($_POST["message"])){
		if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
			die("addresse mail pas correct");
		}

		$message = strip_tags($_POST['message']);
		$email = strip_tags($_POST['email']);

		//On ajoute le nouveau message dans la bdd
		$query = $pdo->prepare("INSERT INTO contact (email,message) VALUES (:email,:message)");
		$query->bindParam(":message", $message);
		$query->bindParam(":email", $email);
		$query->execute();

		header("Location: index.php");

	}
}
