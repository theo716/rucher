<?php
session_start();
if(!isset($_SESSION["utilisateur"])){
    header("Location: connexion.php");
}

include_once "pdo.php";

//Recupere l'id de l'utilisateur
$id_client = $_SESSION['utilisateur']['id'];

//Récupération des stocks de miel dans la bdd
$selectun=$pdo->prepare("SELECT * FROM produit WHERE id_produit=1");
$selectun->execute();
$sun=$selectun->fetch();

$selectdeux=$pdo->prepare("SELECT * FROM produit WHERE id_produit=2");
$selectdeux->execute();
$sdeux=$selectdeux->fetch();

$selecttrois=$pdo->prepare("SELECT * FROM produit WHERE id_produit=3");
$selecttrois->execute();
$strois=$selecttrois->fetch();

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Boutique</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>

    <?php 
    include_once "nav.php";
    ?>
    

    <?php
    if(!empty($_POST)){
        //Vérification des champs de la réservation
        if( ($_POST["q1"] >= 0) && ($_POST["q2"] >= 0) && ($_POST["q3"] >= 0) && (isset($_POST['q1'])) && (isset($_POST['q2'])) && (isset($_POST['q3'])) && isset($_POST['checkbox'])){
            //! Récupération des quantités
            $demandeun = strip_tags($_POST["q1"]);
            $demandedeux = strip_tags($_POST["q2"]);
            $demandetrois = strip_tags($_POST["q3"]);
            $demandetotal = $demandeun + $demandedeux + $demandetrois;

            //Vérification du stock et de la demande
            if(($sun['stock_produit'] < $demandeun) || ($sdeux['stock_produit'] < $demandedeux) || ($strois['stock_produit'] < $demandetrois)){
                $message_echec = "Merci de remplir correctement la commande !";
            }else{

                //Sinon on met à jour la bdd
                $totalun = $sun['stock_produit'] - $demandeun;
                $totaldeux = $sdeux['stock_produit'] - $demandedeux;
                $totaltrois = $strois['stock_produit'] - $demandetrois;

                //On retire du stock la demande
                $count = $pdo->exec("UPDATE produit SET stock_produit=".$totalun." WHERE id_produit=1");
                $count = $pdo->exec("UPDATE produit SET stock_produit=".$totaldeux." WHERE id_produit=2");
                $count = $pdo->exec("UPDATE produit SET stock_produit=".$totaltrois." WHERE id_produit=3");

                //Et on insère la commande dans la table réservation
                $prenom = $_SESSION["utilisateur"]["prenom"];
                $nom_de_famille = $_SESSION["utilisateur"]["nom_de_famille"];
                $count = $pdo->prepare("INSERT INTO reservation (id_client,nom,prenom,q1,q2,q3,total,etat) VALUES (:id_client, :nom_de_famille, :prenom, :demandeun, :demandedeux, :demandetrois, :demandetotal, 0)");
                $count->bindParam(':id_client',$_SESSION['utilisateur']['id']);
                $count->bindParam(':nom_de_famille',$nom_de_famille);
                $count->bindParam(':prenom',$prenom);
                $count->bindParam(':demandeun',$demandeun);
                $count->bindParam(':demandedeux',$demandedeux);
                $count->bindParam(':demandetrois',$demandetrois);
                $count->bindParam(':demandetotal',$demandetotal);
                $count->execute();

                //Recuperation de l'id de la derniere insertion en bdd
                $id_commande = $pdo->lastInsertId();

                header("Location: recu.php?id_commande=$id_commande&id_client=$id_client");
            }
        }else{
            $message_echec = "Merci de remplir correctement la commande !";
        }
    }

    
    ?>

    <div id="reservation">

        <?php 
        if(isset($message_echec)){
        echo "<a class='echec'> $message_echec </a>";
    }
    ?>
        <form id="reservation-form" method="POST" action="">

           <div id="header">
            <div id="titre">
                <h2><?php echo "Bienvenue " . $_SESSION["utilisateur"]["prenom"]. " ".$_SESSION["utilisateur"]["nom_de_famille"]; ?></h2>
            </div>
        </div>
        <div id="formulaire">
            <div id="article-boutique">
                <img src="img/vignette.jpg" alt="miel 250grammes" width="150">
                <label for="q1">Pot de 250 grammes</label>
                <span> Prix unité: <?php echo $sun['prix_produit']; ?>€ </span>
                <span> restant: <?php echo $sun['stock_produit'] ." pots"; ?> </span>
                <input name="q1" id="q1" placeholder="0">
                <br>
            </div>
            <div id="article-boutique">
                <img src="img/vignette.jpg" alt="miel 250grammes" width="150">
                <label for="q2">Pot de 500 grammes</label>
                <span> Prix unité: <?php echo $sdeux['prix_produit']; ?>€ </span>
                <span> restant: <?php echo $sdeux['stock_produit'] ." pots"; ?> </span>
                <input name="q2" id="q2" placeholder="0">
                <br>
            </div>
            <div id="article-boutique">
                <img src="img/vignette.jpg" alt="miel 250grammes" width="150">
                <label for="q3">Pot de 1000 grammes</label>
                <span> Prix unité: <?php echo $strois['prix_produit']; ?>€ </span>
                <span> restant: <?php echo $strois['stock_produit'] . " pots";?> </span>
                <input name="q3" id="q3" placeholder="0">
                <br>
            </div>
        </div>
        <label for="checkbox"><input type="checkbox" name="checkbox"> Vous assurez de pouvoir payer et récuperer la commande.</label>
        
        <div id="btnformulaire">
            <button type="submit">Réserver</button>
        </div>
    </form>
</div>
</body>
</html>
