<?php
try{
    //! Connexion à la base de données sqlite
    $pdo = new PDO('sqlite:rucher.db');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
    echo "Probleme à la connexion";
    die();
}

?>
