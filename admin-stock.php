<?php
  
  // Lancement de la session avec vérification de si l'utilisateur à les droits admin
  session_start();
  if ($_SESSION["utilisateur"]["statut"] != 1){
      header("Location: index.php");
  }
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>Panel Admin</title>
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    
    <?php
      include_once "nav_admin.php";
      include_once "pdo.php";
    ?>
    <div id="admin-conteneur">
      <div id="admin-header">
        <?php include_once "onglet-admin.php"; ?>
        <div id="conteneur-stock-admin">
          <div id="admin-stock" class="tab">
            <h2>Stock</h2>
            <table>
              <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Code</th>
                <th>Stock</th>
                <?php 
                    
                    // Récupération de l'ensemble de la table produit
                    // et affichage dans un tableau
                    
                    $sql = "SELECT * FROM produit";
                  
                  foreach ($pdo->query($sql) as $row) {
                      echo "<tr>";
                      print '<td>' . $row['id_produit'] . '</td>';
                      print '<td>' . $row['nom_produit'] . '</td>';
                      print '<td>' . $row['code_produit'] . '</td>';
                      print '<td>' . $row['stock_produit'] . '</td>';
                      echo "</tr>";
                  }
                ?>
              </tr>
            </table>
          </div>
          <div id="admin-stock" class="tab">
            <h2>Ajouter du stock:</h2>
            <form action="" method="POST">
              <label for="ajoutstock1"></label>
              <input type="text" name="ajoutstock1" id="ajoutstock1" placeholder="250g"></input>
              <br>
              <label for="ajoutstock2"></label>
              <input name="ajoutstock2" id="ajoutstock2" placeholder="500g"></input>
              <br>
              <label for="ajoutstock3"></label>
              <input name="ajoutstock3" id="ajoutstock3" placeholder="1000g"></input>
              <button type="submit">Ajouter</button>
            </form>
          </div>
          
          <div id="admin-stock" class="tab">
            <h2>Retirer du stock:</h2>
            <form action="" method="POST">
              <label for="retirestock1"></label>
              <input type="text" name="retirestock1" id="retirestock1" placeholder="250g"></input>
              <br>
              <label for="retirestock2"></label>
              <input name="retirestock2" id="retirestock2" placeholder="500g"></input>
              <br>
              <label for="retirestock3"></label>
              <input name="retirestock3" id="retirestock3" placeholder="1000g"></input>
              <button type="submit">Retirer</button>
            </form>
          </div>
        </div>
        <?php
              
              //Permet de retirer / ajouter du stock à la bdd
              //Récupération des stocks dans des variables
              $select_un=$pdo->prepare("SELECT stock_produit AS nombre FROM produit WHERE id_produit=1");
          $select_un->execute();
          $s_un=$select_un->fetch(PDO::FETCH_OBJ);
          
          $select_deux=$pdo->prepare("SELECT stock_produit AS nombre FROM produit WHERE id_produit=2");
          $select_deux->execute();
          $s_deux=$select_deux->fetch(PDO::FETCH_OBJ);
          
          $select_trois=$pdo->prepare("SELECT stock_produit AS nombre FROM produit WHERE id_produit=3");
          $select_trois->execute();
          $s_trois=$select_trois->fetch(PDO::FETCH_OBJ);
          
          if(!empty($_POST["ajoutstock1"]) && !empty($_POST["ajoutstock2"]) && !empty($_POST["ajoutstock3"])){

              //On effectue le stock de base + l'ajout
              $totalun = $s_un->nombre + $_POST["ajoutstock1"];
              $totaldeux = $s_deux->nombre + $_POST["ajoutstock2"];
              $totaltrois = $s_trois->nombre + $_POST["ajoutstock3"];
              
              //On met à jour la bdd pour l'ajout du stock
              $count = $pdo->prepare("UPDATE produit SET stock_produit= :totalun  WHERE id_produit=1");
              $count->bindParam(':totalun',$totalun);
              $count->execute();
              
              $count = $pdo->prepare("UPDATE produit SET stock_produit= :totaldeux WHERE id_produit=2");
              $count->bindParam(':totaldeux',$totaldeux);
              $count->execute();
              
              $count = $pdo->prepare("UPDATE produit SET stock_produit= :totaltrois WHERE id_produit=3");
              $count->bindParam(':totaltrois',$totaltrois);
              $count->execute();

              header("Location: admin-stock.php");
          }
          
          
          if(!empty($_POST["retirestock1"]) && !empty($_POST["retirestock2"]) && !empty($_POST["retirestock3"])){
 
              //On effectue le stock de base + l'ajout
              $totalun = $s_un->nombre - $_POST["retirestock1"];
              $totaldeux = $s_deux->nombre - $_POST["retirestock2"];
              $totaltrois = $s_trois->nombre - $_POST["retirestock3"];
              
              //On met à jour la bdd pour l'ajout du stock
              $count = $pdo->prepare("UPDATE produit SET stock_produit= :totalun  WHERE id_produit=1");
              $count->bindParam(':totalun',$totalun);
              $count->execute();
              
              $count = $pdo->prepare("UPDATE produit SET stock_produit= :totaldeux WHERE id_produit=2");
              $count->bindParam(':totaldeux',$totaldeux);
              $count->execute();
              
              $count = $pdo->prepare("UPDATE produit SET stock_produit= :totaltrois WHERE id_produit=3");
              $count->bindParam(':totaltrois',$totaltrois);
              $count->execute();

              header("Location: admin-stock.php");
          }
        ?>
      </div>
  </body>
</html>
