<?php

session_start();

include_once "pdo.php";

if (!empty($_POST)) {
	if (isset($_POST['mdp'], $_POST['remdp']) && !empty($_POST['mdp']) && !empty($_POST['remdp'])) {
		$mdp = strip_tags($_POST['mdp']);
		$remdp = strip_tags($_POST['remdp']);
		if ($mdp != $remdp) {
			$message_echec = "Merci de mettre le même mot de passe!";
		} else {
			//hash le nouveau mot de passe
			$mdp = password_hash($mdp, PASSWORD_ARGON2ID);

			//On modifie le mot de passe de l'utilisateur
			$sql = $pdo->prepare("UPDATE utilisateurs SET pass = :mdp WHERE id = :id_utilisateur");
			$sql->bindParam(':mdp', $mdp);
			$sql->bindParam(':id_utilisateur', $_SESSION['utilisateur']['id']);
			$sql->execute();

			header("Location: profil.php");
		}
	}
}




?>
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF-8">
	<title>Votre profil</title>
	<link rel="stylesheet" href="styles.css">
</head>
<?php include_once "nav.php"; ?>

<body>

	<div id="conteneur-profil">
		<h2>Votre profil</h2>
		<div id="conteneur-log-mdp">

			<form id="modif-profil" method="post">
				<div id="nouveau-mdp">
					<label for="mdp">Nouveau mot de passe*: </label>
					<input type="password" id="mdp" name="mdp" require></input>
				</div>

				<div id="nouveau-mdp">
					<label for="remdp">Confirmation mot de passe*: </label>
					<input type="password" id="remdp" name="remdp" require></input>
				</div>

				<button id="bouton-nouveau-mdp" type="submit">Modifier</button>
				<?php
				if (isset($message_echec)) {
					echo "<a class='echec'> $message_echec </a>";
				}
				?>
			</form>
		</div>
	</div>
</body>

</html>