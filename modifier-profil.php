<?php
session_start();

include_once "pdo.php";

if (!empty($_POST)) {
	if (isset($_POST['nom'], $_POST['prenom'], $_POST['email']) && !empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['email'])) {

		if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
			die("addresse mail pas correct");
		}

		$prenom = strip_tags($_POST['prenom']);
		$nom_de_famille = strip_tags($_POST['nom']);

		$sql = $pdo->prepare("UPDATE utilisateurs SET prenom = :prenom_utilisateur, nom_de_famille = :nom_de_famille, email = :email_utilisateur WHERE id = :id_utilisateur");
		$sql->bindParam(':prenom_utilisateur', $prenom);
		$sql->bindParam(':nom_de_famille', $nom_de_famille);
		$sql->bindParam(':id_utilisateur', $_SESSION['utilisateur']['id']);
		$sql->bindParam(':email_utilisateur', $_POST['email']);
		$sql->execute();

		$query = $pdo->prepare("SELECT * FROM utilisateurs WHERE id = :id_utilisateur");
		$query->bindParam(":id_utilisateur", $_SESSION['utilisateur']['id']);
		$query->execute();
		$utilisateur = $query->fetch();

		$_SESSION['utilisateur'] = [
			"id" => $utilisateur["id"],
			"prenom" => $utilisateur["prenom"],
			"nom_de_famille" => $utilisateur["nom_de_famille"],
			"email" => $utilisateur["email"],
			"statut" => $utilisateur["statut"]
		];

		header("Location: profil.php");
	}
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF-8">
	<title>Votre profil</title>
	<link rel="stylesheet" href="styles.css">
</head>
<?php include_once "nav.php"; ?>

<body>

	<div id="conteneur-profil">
		<h2>Votre profil</h2>
		<div id="conteneur-log-mdp">

			<form id="modif-profil" method="post">
				<div id="nouveau-mdp">
					<label for="nom">Nom*: </label>
					<input type="text" id="nom" name="nom" value="<?php echo $_SESSION['utilisateur']['nom_de_famille']; ?>" require></input>
				</div>

				<div id="nouveau-mdp">
					<label for="prenom">Prenom*: </label>
					<input type="text" id="prenom" name="prenom" value="<?php echo $_SESSION['utilisateur']['prenom']; ?>" require></input>
				</div>


				<div id="nouveau-mdp">
					<label for="email">Email*: </label>
					<input type="email" id="email" name="email" value="<?php echo $_SESSION['utilisateur']['email']; ?>" require></input>
				</div>

				<button id="bouton-nouveau-mdp" type="submit">Modifier</button>

			</form>
		</div>
	</div>
</body>
</html>