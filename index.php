<?php
session_start();

include_once "pdo.php";
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Rucher du Pillier</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
    <?php include_once "nav.php" ?>
    <header>
        <div id="banniere">
            <h1>Le <strong>Rucher</strong></h1>
            <h1>du <strong>Pillier</strong></h1>
            <a id="button-nav-banniere" href="boutique.php">🐝 Réserver du miel 🐝</a>
        </div>
    </header>
    <div id="presentation">
        <div id="presentation-conteneur">
            <div id="presentation-texte">
                <p>
                    Rucher du <strong>PILLIER</strong> à Châteauneuf Loire 42 se situe de la commune de <strong>CHATEAUNEUF</strong> au bord du parc naturel du Pilat sur le versant nord à la limite du département du <strong>RHONE</strong> lieu dit <strong>DIZIMIEUX</strong>, il a la chance de se situer au cœur d’une exploitation agricole <strong>FONT</strong> Jean–pierre qui privilégie une agriculture biologique respectueuse de l’environnement.
                    <br><br>
                    Les abeilles ont à leur disposition du début du printemps à la fin de l’été : églantiers, arbres fruitiers, trèfles, acacias, châtaigniers, muriers. Certaines années elles trouveront : les fleurs de moutarde, phacélies utilisées comme engrais vert par notre agriculteur et d’innombrables fleurs des champs.
                    <br><br>
                    Le rucher du <strong>PILLIER</strong> est à proximité d’un chemin de randonnée qui peut aller de la chartreuse de <strong>SAINT CROIX EN JARREZ</strong> à <strong>MADINAY</strong> en passant par les roches de Merlin .Vous aurez l’occasion d’apercevoir le rucher qui se trouve en bordure de forêt : attention, garder vos distances, vous êtes peut être allergiques aux piqûres.
                </p>
            </div>
            <div id="diapo">
                <img class="hide" src="img/diapo1.png" alt="image" height="300" width="300" id="img-diapo">
            </div>
        </div>
    </div>

    <div id="conteneur-dernier-article">
        <h2>Derniers articles en ligne: </h2>
        <div id="derniers-articles">
            <?php
            $article = $pdo->query("SELECT * FROM article ORDER BY id DESC LIMIT 3");
            while ($a = $article->fetch()) {
            ?>
                <div id="article-seul-index">
                    <a href="article.php?id=<?php echo $a['id']; ?>"> <?php echo $a["titre"] ?></a>
                    <br>
                    <span>Par <?php echo $a['createur']; ?>, le <?php echo $a['date_creation']; ?>.</span>
                </div>
                <?php
            }
                ?>
        </div>
    </div>

    <div id="conteneur-global-contact">
        <div id="conteneur-contact">

            <div id="contact-form">
                <form method="POST" action="ajout-contact.php" id="contact">
                    <label for="email">Email *</label>
                    <input type="email" id="email" name="email" placeholder="Votre email .." required>

                    <label for="message">Message *</label>
                    <textarea id="message" name="message" placeholder="Votre message .." style="height:100px" required></textarea>
                    <button type="submit">Envoyer</button>
                </form>
            </div>
        </div>
        <div id="google-map-integer">
        <iframe class="hide" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d932.8254750934968!2d4.642782637898159!3d45.52380279408618!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47f4fd40533ad809%3A0x6fc6fae4f8311908!2sRucher%20du%20pillier!5e0!3m2!1sfr!2sfr!4v1667119136755!5m2!1sfr!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
    </div>
</body>
<?php include_once "footer.php" ?>
<script>
    var diapo = new Array("img/diapo1.png", "img/diapo2.png", "img/diapo3.png");
    var i = 0;

    setInterval("diapoSuivant(1)", 3000);

    function diapoSuivant(y) {
        i = i + y;
        if (i < 0) {
            numero = diapo.length - 1;
        }
        if (i > diapo.length - 1) {
            i = 0;
        }
        document.getElementById("img-diapo").src = diapo[i];
    }
</script>

</html>