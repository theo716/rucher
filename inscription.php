<?php
session_start();

require_once "pdo.php";
if(!empty($_SESSION)){
	header("Location: connexion.php");
}

//Vérifie si le formulaire n'est pas vide
if(!empty($_POST)){
	//Vérifie  qu'il y aun pseudo, un email et un mot de passe et que les champs ne sont pas vides
	if(isset($_POST["prenom"],$_POST["nom_de_famille"], $_POST["email"], $_POST["pass"], $_POST["repass"]) && !empty($_POST["prenom"]) && !empty($_POST["nom_de_famille"]) && !empty($_POST["email"]) && !empty($_POST["pass"]) && !empty($_POST["repass"])){

		//On retire toutes balises HTML dans le pseudo
		$prenom = strip_tags($_POST["prenom"]);
		$nom_de_famille = strip_tags($_POST["nom_de_famille"]);

		//Statut type d'un membre qui vient de s'inscrire
		$statut = 0;

		//Vérification de l'email avec la méthode FILTER
		if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
			$message_echec = "Erreur adresse email!";	
		}else{

			if($_POST["pass"] != $_POST["repass"]){
				$message_echec = "Merci de taper le même mot de passe";
			}else{

				//! ON HASH LE MOT DE PASSE
				$pass = password_hash($_POST["pass"], PASSWORD_ARGON2ID);

				
				//Email déja utilisé ?
				$email = strip_tags($_POST["email"]);
				$query = $pdo->prepare("SELECT * FROM utilisateurs WHERE email = :email");
				$query->bindParam(":email",$email);
				$query->execute();
				$utilisateur = $query->fetch();

				//Si l'utilisateur n'est pas en base de données alors on stop
				if($utilisateur){
					$message_echec = "Email déja utilisé";
				}else{

					//On ajoute le nouveau utilisateur dans la base de données
					$sql = "INSERT INTO utilisateurs (prenom,nom_de_famille,email,pass,statut) VALUES (:prenom,:nom_de_famille, :email, :pass,:statut)";
					$query = $pdo->prepare($sql);
					$query->bindParam(":prenom", $prenom);
					$query->bindParam(":nom_de_famille", $nom_de_famille);
					$query->bindParam(":email",$_POST["email"]);
					$query->bindParam(":statut",$statut);
					$query->bindParam(":pass",$pass);
					$query->execute();

					$id = $pdo->lastInsertId();

					//On stock en SESSION les informations nécessaires
					$_SESSION["utilisateur"] = [
						"id" => $id,
						"prenom" => $prenom,
						"nom_de_famille" => $nom_de_famille,
						"email" => strip_tags($_POST["email"]),
						"statut" => $statut
					];

					//On redirige vers la page après l'inscription
					if($_SESSION["utilisateur"]["statut"] == 1){
						header("Location: admin.php");
					}else{
						header("Location: profil.php");
					}
				}
			}
		}
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Inscription</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
<?php include_once "nav.php"; ?>
    <div id="conteneur-connexion-inscription">
    <h1>Inscription</h1>
    <form method="post">
	<div class="nom">
	<label for="prenom">Prénom *</label>
	<input type="text" id="prenom" name="prenom" required></input>
	</div>

	<div class="nom_de_famille">
	<label for="nom_de_famille">Nom de famille *</label>
	<input type="text" id="nom_de_famille" name="nom_de_famille" required></input>
	</div>

	<div class="email">
	<label for="email">Email *</label>
	<input type="email" id="email" name="email" required></input>
	</div>

	<div class="pass">
	<label for="pass">Mot de passe *</label>
	<input type="password" id="pass" name="pass" require></input>
	</div>

	<div class="repass">
	<label for="repass">Mot de passe *</label>
	<input type="password" id="repass" name="repass" require></input>
	</div>

	<button type="submit">Inscription</button>
    </form>
    <a>Déjà inscris ? </a><a href="connexion.php">connectez-vous</a>


<?php
if(isset($message_echec)){
	echo "<a class='echec'> $message_echec </a>";
}
if(isset($message_succes)){
	echo "<a class='succes'> $message_succes </a>";
}
?>
    </div>
</body>
</html>
