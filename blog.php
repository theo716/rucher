<?php
session_start();
?>


<?php
include_once "nav.php";
include_once "pdo.php";

$articles = $pdo->query("SELECT * FROM article ORDER BY id DESC");

function chaineCoupee($contenu)
{
  $length = 75;
  if (strlen($contenu) > $length) {
    $chaineCoupee = substr($contenu, 0, $length) . '...'; 
    return $chaineCoupee;
  }
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Rucher du Pillier</title>
  <link rel="stylesheet" href="styles.css">
</head>

<body>
  <div id="conteneur-article">
    <h2>Les dernières actualités du Rucher du Pillier:</h2>
    <?php while ($a = $articles->fetch()) { ?>
      <div id="article-seul">
        <a id="article-titre" href="article.php?id=<?php echo $a['id']; ?>"> <?php echo $a["titre"] ?></a>
        <p id="article-desc">
          <?php
          $b = chaineCoupee($a['contenu']);
          echo $b;
          ?>
          <br>
          <a id="article-suite" href="article.php?id=<?php echo $a['id']; ?>">Lire la suite</a>
        </p>
        <span>Par <?php echo $a['createur']; ?> le <?php echo $a['date_creation']; ?></span>
      </div>
      <?php if ($_SESSION["utilisateur"]["statut"] == 1) { ?>
        <a href="admin-article.php?modifier=<?php echo $a['id']; ?>">Modifier</a>
        <a href="supprimer-blog.php?id=<?php echo $a['id']; ?>">Supprimer</a><br>
    <?php
      }
    }
    ?>
  </div>
</body>

</html>