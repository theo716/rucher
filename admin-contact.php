<?php

session_start();

//Verification si l'utilisateur est admin
if ($_SESSION["utilisateur"]["statut"] != 1){
	header("Location: index.php");
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Panel Admin</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>

<?php
include_once "nav_admin.php";
include_once "pdo.php";
?>
<div id="admin-conteneur">
    <div id="admin-header">
    <?php include_once "onglet-admin.php"; ?>
    </div>


<?php 

//Récuperation de l'ensemble des messages du contact
$sql = "SELECT * FROM contact";

foreach ($pdo->query($sql) as $row) {
	echo '<div id="message-unique">';?>
    <td><a href="supprimer-message.php?id=<?php echo $row['id']; ?>">Supprimer</a></td><?php
	echo '<span class="contact-section"><h4>Email:</h4>' . $row['email'] . '</span><br>';
	echo '<p class="contact-section"><h4>Message:</h4>' . $row['message'] . '</p>';
    echo '</div>';
}
echo "</tr>";
?>

</div>
</body>
</html>
