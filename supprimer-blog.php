<?php 
session_start();

if ($_SESSION["utilisateur"]["statut"] != 1){
	header("Location: index.php");
}

include_once "pdo.php";

if (isset($_GET['id']) && !empty($_GET['id'])) {
	$supprimer_id = strip_tags($_GET['id']);

	$supprimer_article = $pdo->prepare('DELETE FROM article WHERE id = :id');
	$supprimer_article->bindParam(":id",$supprimer_id);
	$supprimer_article->execute();

	header("Location: blog.php");
}
?>