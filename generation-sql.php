<?php
include_once "pdo.php";

$table = "CREATE TABLE article (
id	INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
titre	TEXT NOT NULL,
contenu	TEXT NOT NULL,
createur	TEXT NOT NULL,
date_creation	DATETIME,
date_modification	DATETIME
)";

$pdo->exec($table);

$table = "CREATE TABLE contact (
id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
email	TEXT NOT NULL,
message	TEXT NOT NULL
)";

$pdo->exec($table);

$table = "CREATE TABLE produit (
id_produit	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
nom_produit	TEXT NOT NULL,
prix_produit	INTEGER NOT NULL,
code_produit	INTEGER NOT NULL,
stock_produit	INTEGER NOT NULL
)";

$pdo->exec($table);

$table = "CREATE TABLE reservation (
id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
id_client	INTEGER NOT NULL,
nom	TEXT NOT NULL,
prenom	TEXT NOT NULL,
q1	INTEGER NOT NULL,
q2	INTEGER NOT NULL,
q3	INTEGER NOT NULL,
total	INTEGER NOT NULL,
etat	INTEGER NOT NULL
)";

$pdo->exec($table);

$table = "CREATE TABLE utilisateurs (
id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
prenom	TEXT NOT NULL,
nom_de_famille	TEXT NOT NULL,
email	TEXT NOT NULL,
pass	TEXT NOT NULL,
statut	INTEGER NOT NULL
)";

$pdo->exec($table);

$pdo->exec('INSERT INTO utilisateurs(prenom,nom_de_famille,email,pass,statut) VALUES ("admin","admin","admin@admin.admin","$argon2id$v=19$m=65536,t=4,p=1$UkRqd1p1YVB5UGI5Yi4zaA$2UWCIJ6eB9aJe6Z8Vg4mXiki3fnYVKzCEJrGT0OqRAw","1")');
$pdo->exec("INSERT INTO produit(id_produit,nom_produit,prix_produit,code_produit,stock_produit) VALUES ('1','250g','10','100','0')");
$pdo->exec("INSERT INTO produit(id_produit,nom_produit,prix_produit,code_produit,stock_produit) VALUES ('2','500g','15','101','0')");
$pdo->exec("INSERT INTO produit(id_produit,nom_produit,prix_produit,code_produit,stock_produit) VALUES ('3','1000g','20','102','0')");

$pdo->exec("INSERT INTO article(titre,contenu,createur,date_creation) VALUES ('Ouverture du site !','Bienvenue sur la première version du site !','admin',date())");
$pdo->exec("INSERT INTO article(titre,contenu,createur,date_creation) VALUES ('Ouverture des ventes en Septembre 2022 !','Il sera possible dobtenir votre miel en septembre au moment de la récolte !','admin',date())");
?>