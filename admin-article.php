<?php 
  session_start();
  
  //Vérification si l'utilisateur est admin
  if ($_SESSION["utilisateur"]["statut"] != 1){
      header("Location: index.php");
  }
  
  include_once "pdo.php";
  
  //Mode d'édition
  $edition = 0;

  //Vérification du champs modifier soit pas vide et nul
  if (isset($_GET['modifier']) && !empty($_GET['modifier'])) {
      //On rentre dans le mode édition
      $edition = 1;
      
      //On recupere l'id de l'article à modifier
      $modification_id = htmlspecialchars($_GET['modifier']);
      
      $query = $pdo->prepare("SELECT * FROM article WHERE id = :id_article");
      $query->bindParam(":id_article",$modification_id);
      $query->execute();
      $article = $query->fetch();

      
  }
  
  if(!empty($_POST)){
      //Si le titre et le contenu est bon
      if(isset($_POST['titre']) && isset($_POST['contenu'])){
          
          //On récupere le titre et le contenu avec la sécurité de htmlspecialchars
          $titre = htmlspecialchars($_POST['titre']);
          $contenu = htmlspecialchars($_POST['contenu']);
          
          //Si le mode édition n'est pas actif on ajoute l'article dans la BDD
          if ($edition == 0) {
              $count = $pdo->prepare("INSERT INTO article (titre, contenu,createur,date_creation) VALUES (:titre, :contenu,:createur,date())");
              $count->bindParam(':titre',$titre);
              $count->bindParam(':contenu',$contenu);
              $count->bindParam(':createur',$_SESSION['utilisateur']['prenom']);
              $count->execute();
              
              $message = "Votre article à bien été posté";
              
              //Sinon si le mode edition est actif, on met à jour l'article
          }else{
              $update = $pdo->prepare("UPDATE article SET titre = :titre, contenu = :contenu, date_modification = date() WHERE id = :id");
              $update->bindParam(':titre',$titre);
              $update->bindParam(':contenu',$contenu);
              $update->bindParam(':id',$modification_id);
              $update->execute();
              header("Location: blog.php");
          }
          
          
      }
  }
  
  
  
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>Panel Admin</title>
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    
    <?php
      include_once "nav_admin.php";     
    ?>
    
    
    <div id="admin-conteneur">
      <?php include_once "onglet-admin.php"; ?>
       
       
       <form action="" method="POST" id="form-article">
	 <h2>Rédiger un nouvel article !</h2>
         <label for="titre">Titre</label>
         <?php if($edition == 1){?>
          <input type="text" name="titre" id="titre" value="<?php echo $article["titre"]; ?>" required></input>
          <?php }else{?>
           <input type="text" name="titre" id="titre" placeholder="Votre titre .. " required></input>
           <?php } ?>
            <br>
            <label for="contenu">Contenu de l'article</label>
            <?php if($edition == 1){ ?>
             <textarea id="contenu" name="contenu"required><?php echo $article["contenu"]; ?></textarea>
             <?php }else{?>
              <textarea id="contenu" name="contenu" placeholder="Votre article .. " required></textarea>
              <?php } ?>
               <br>
               
               <button type="submit">Publier</button>
       </form>
       <br>
       
       <?php if(isset($message)){
    echo "<a class='succes'> $message </a>";
}
?>

</div>


</body>
</html>
