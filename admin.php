<?php

session_start();
if ($_SESSION["utilisateur"]["statut"] != 1){
	header("Location: index.php");
}

include_once "pdo.php";

//Recuperation du nombre de réservation à verifier
$select=$pdo->prepare("SELECT COUNT(*) AS nombre FROM reservation WHERE etat = 0");
$select->execute();
$s=$select->fetch(PDO::FETCH_OBJ);

//Recuperation du nombre de message à lire
$select=$pdo->prepare("SELECT COUNT(*) AS nombre FROM contact");
$select->execute();
$d=$select->fetch(PDO::FETCH_OBJ);

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Panel Admin</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>

<?php
include_once "nav_admin.php";
?>
<div id="admin-conteneur">
<?php include_once "onglet-admin.php"; ?>
<h1>Panel administration</h1>
<p>Il y a <?php echo $s->nombre; ?> commande(s) à vérifier.</p>
<p>Il y a <?php echo $d->nombre; ?> nouveaux message(s).</p>
</div>
</body>
</html>
