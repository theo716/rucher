<?php

//Lancement de la session avec vérifications des droits de la personne

session_start();
if ($_SESSION["utilisateur"]["statut"] != 1){
    header("Location: index.php");
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Panel Admin</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>

    <?php
    include_once "nav_admin.php";
    include_once "pdo.php";
    ?>
<div id="admin-conteneur">
<?php include_once "onglet-admin.php"; ?>
    <div id="admin-header">
    <div id="admin-reservation">
<div id="stock-section">
<h2>Nombres de réservations: 
            <?php
            $select=$pdo->prepare("SELECT COUNT(*) AS nombre FROM reservation");
            $select->execute();
            $s=$select->fetch(PDO::FETCH_OBJ);
            echo $s->nombre;
            ?>
        </h2>
       
</div>
<div id="stock-section">
        <h2>Réservations à vérifier:</h2>
        <table>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>250g</th>
                <th>500g</th>
                <th>1000g</th>
                <th>Total</th>
                <th>Etat</th>
                <th>Vérifier</th>
                <?php 
                    $sql = "SELECT * FROM reservation";
                    
                    foreach ($pdo->query($sql) as $row) {
                        if($row['etat'] == 0){
                        echo "<tr>";
                        print '<td>' . $row['id'] . '</td>';
                        print '<td>' . $row['nom'] . '</td>';
                        print '<td>' . $row['prenom'] . '</td>';
                        print '<td>' . $row['q1'] . '</td>';
                        print '<td>' . $row['q2'] . '</td>';
                        print '<td>' . $row['q3'] . '</td>';
                        print '<td>' . $row['total'] . '</td>';
                        if($row['etat'] == 0){
                            print '<td class="verification">à vérifier</td>';
                        }
                        ?> 
                        <td class="milieu"><a href="verification.php?id=<?php echo $row['id'];?>">V</a></td><?php
                        }
                    }
                    echo "</tr>";
                    
                ?>
            </tr>
        </table>

</div>
<div id="stock-section">
        <h2>Réservations en cours:</h2>
        <table>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>250g</th>
                <th>500g</th>
                <th>1000g</th>
                <th>Total</th>
                <th>Etat</th>
                <th>Valider</th>
                <?php 
                    $sql = "SELECT * FROM reservation";
                    
                    foreach ($pdo->query($sql) as $row) {
                        if($row['etat'] == 1){
                        echo "<tr>";
                        print '<td>' . $row['id'] . '</td>';
                        print '<td>' . $row['nom'] . '</td>';
                        print '<td>' . $row['prenom'] . '</td>';
                        print '<td>' . $row['q1'] . '</td>';
                        print '<td>' . $row['q2'] . '</td>';
                        print '<td>' . $row['q3'] . '</td>';
                        print '<td>' . $row['total'] . '</td>';
                        if($row['etat'] == 1){
                            print '<td class="cours">en cours</td>    ';
                        }
                    ?> 
                    <td class="milieu"> <a href="validation.php?id=<?php echo $row['id'];?>">V</a></td><?php
                        }
                    }
                    echo "</tr>";
                    
                ?>
            </tr>
        </table>
</div>

<div id="stock-section">
        <h2>Réservations terminées:</h2>
        <table>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>250g</th>
                <th>500g</th>
                <th>1000g</th>
                <th>Total</th>
                <?php 
                    $sql = "SELECT * FROM reservation";
                    
                    foreach ($pdo->query($sql) as $row) {
                        if($row['etat'] == 2){
                        echo "<tr>";
                        print '<td class="valide">' . $row['id'] . '</td>';
                        print '<td class="valide">' . $row['nom'] . '</td>';
                        print '<td class="valide">' . $row['prenom'] . '</td>';
                        print '<td class="valide">' . $row['q1'] . '</td>';
                        print '<td class="valide">' . $row['q2'] . '</td>';
                        print '<td class="valide">' . $row['q3'] . '</td>';
                        print '<td class="valide">' . $row['total'] . '</td>';
                        }
                    }
                    echo "</tr>";
                    
                ?>
            </tr>
        </table>
</div>
<div id="stock-section">
 <h2>Toutes les réservations:</h2>
        <table>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>250g</th>
                <th>500g</th>
                <th>1000g</th>
                <th>Total</th>
		<th>Etat</th>
		<th>Supprimer</th>
                <?php 
                    $sql = "SELECT * FROM reservation";
                    
                    foreach ($pdo->query($sql) as $row) {
                    echo "<tr>";
                    print '<td>' . $row['id'] . '</td>';
                    print '<td>' . $row['nom'] . '</td>';
                    print '<td>' . $row['prenom'] . '</td>';
                    print '<td>' . $row['q1'] . '</td>';
                    print '<td>' . $row['q2'] . '</td>';
                    print '<td>' . $row['q3'] . '</td>';
		    print '<td>' . $row['total'] . '</td>';
                    if($row['etat'] == 0){
                        print '<td class="verification">à vérifier</td>';
                    }
                    if($row['etat'] == 1){
                        print '<td class="cours">en cours</td>';
                    }
                    if($row['etat'] == 2){
                        print '<td class="valide">terminé</td>';
                    }
                    
        		    print '<td><a href="supprimer.php?id=' . $row['id'] . '">X</a></td>'; 
        		    }
                    echo "</tr>";
                    
                ?>
            </tr>
    </table>
    </div>
    </div>
    </div>
