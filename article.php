<?php
session_start();
include_once "pdo.php";
include_once "nav.php";

$id_article = strip_tags($_GET['id']);

$query = $pdo->prepare('SELECT * FROM article WHERE id = :id_article');
$query->bindValue(":id_article",$id_article);
$query->execute();
$article = $query->fetch();



if($article){
if (isset($_GET["id"]) && !empty($_GET["id"])){

        $titre = $article["titre"];
        $contenu = $article["contenu"];
}
}else{
    header("Location: blog.php");
}
?>

<!DOCTYPE html>
<html>
  <head>
    <title>article</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    <div id="article-global">
      <h1><?php echo $titre ?></h1>
      <p><?php echo $contenu ?></p>
      <h3>Rédigé par <?php echo $article['createur']; ?>, le <?php echo $article['date_creation']; ?>.</h3>
      <?php
if($article['date_modification']){
    ?> <h4> Modifié le <?php echo $article['date_modification']; ?>.</h4><?php
}
?>
    </div>
  </body>
</html>
